package com.walruspay.assignment.twitter_clone.data.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "USER")
@Entity
@NoArgsConstructor
@Getter
@Setter
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "USER_ID")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer userId;
	@Column(name = "NAME", nullable = false)
	private String name;
	@Column(name = "MOBILE")
	private String mobile;
	@Column(name = "EMAIL")
	private String email;
	@Column(name = "PASSWORD") // encoded 
	private String password;
	@Column(name = "WRONG_ATTEMPS")
	private Integer wrongAttempts;
	@CreationTimestamp
	@Column(name = "CREATED_ON", nullable = false)
	private Timestamp createdOn;
	@UpdateTimestamp
	@Column(name = "UPDATED_ON", nullable = false)
	private Timestamp updatedOn;

	public User(String name, String mobile, String email, String password) {
		this.name = name;
		this.mobile = mobile;
		this.email = email;
		this.password = password;
	}
	
}
