package com.walruspay.assignment.twitter_clone.data.models;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "SESSION")
@Entity
@NoArgsConstructor
@Getter
@Setter
public class SessionDetails implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "SESSION_ID")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer sessionId;
	@Column(name = "SESSION_KEY", nullable = false)
	private String sessionKey;
	@Column(name = "USER_ID")
	private String userId;
	@Column(name = "VALID_TILL", nullable = false)
	private Date validTill;
	@Column(name = "IS_ACTIVE", nullable = false)
	private Boolean isActive;
	@CreationTimestamp
	@Column(name = "CREATED_ON", nullable = false)
	private Timestamp createdOn;
	@UpdateTimestamp
	@Column(name = "UPDATED_ON", nullable = false)
	private Timestamp updatedOn;

	public SessionDetails(String session, Date validTill, Boolean isActive) {
		this.sessionKey = session;
		this.validTill = validTill;
		this.isActive = isActive;
	}

}
