package com.walruspay.assignment.twitter_clone.service;

import java.util.List;

import com.walruspay.assignment.twitter_clone.models.FetchPostRequest;
import com.walruspay.assignment.twitter_clone.models.PostRequest;
import com.walruspay.assignment.twitter_clone.models.PostResponse;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;

public interface PostService {

	ServiceResponse<String> createPost(PostRequest request);

	ServiceResponse<List<PostResponse>> fetchAllPost(FetchPostRequest request);
}
