package com.walruspay.assignment.twitter_clone.dao;

import com.walruspay.assignment.twitter_clone.data.models.Reaction;

public interface ReactionDao {

	Reaction getReactionByUserIdAndPostId(Integer useId, Integer postId);

}
