package com.walruspay.assignment.twitter_clone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.walruspay.assignment.twitter_clone.models.ServiceRequest;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;
import com.walruspay.assignment.twitter_clone.models.ValidateSessionRequest;
import com.walruspay.assignment.twitter_clone.service.SessionService;

@RestController
@RequestMapping(value="v1/session")
public class SessionController {
	@Autowired
	private SessionService sessionService;

	@GetMapping("generate")
	public ServiceResponse<String> generateSession() {
		return sessionService.generateSession();
	}
	
	@PostMapping("validate")
	public ServiceResponse<String> validateSession(@RequestBody ServiceRequest<ValidateSessionRequest> resquest){
		return sessionService.vaildateSession(resquest.getData());
	}
}
