package com.walruspay.assignment.twitter_clone.service;

import com.walruspay.assignment.twitter_clone.models.FollowerRequest;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;

public interface FollowerService {

	public ServiceResponse<String> addFollower(FollowerRequest request);

}
