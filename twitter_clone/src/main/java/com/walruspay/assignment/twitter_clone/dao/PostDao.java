package com.walruspay.assignment.twitter_clone.dao;

import java.util.List;

import com.walruspay.assignment.twitter_clone.data.models.PostDeatils;

public interface PostDao {

	PostDeatils getPostByUserIdAndPostId(Integer userId, Integer postId);

	List<PostDeatils> getAllPostsByUserId(Integer userId);

}
