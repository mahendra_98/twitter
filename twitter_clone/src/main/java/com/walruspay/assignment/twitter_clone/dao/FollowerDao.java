package com.walruspay.assignment.twitter_clone.dao;

import com.walruspay.assignment.twitter_clone.data.models.Followers;

public interface FollowerDao {

	Followers getFollowerByUserIdAndFollowerId(Integer userId, Integer followerId);

}
