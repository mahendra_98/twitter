package com.walruspay.assignment.twitter_clone.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walruspay.assignment.twitter_clone.dao.CommonDao;
import com.walruspay.assignment.twitter_clone.dao.FollowerDao;
import com.walruspay.assignment.twitter_clone.dao.UserDao;
import com.walruspay.assignment.twitter_clone.data.models.Followers;
import com.walruspay.assignment.twitter_clone.models.FollowerRequest;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;
import com.walruspay.assignment.twitter_clone.service.FollowerService;
import com.walruspay.assignment.twitter_clone.service.impl.helper.FollowerServiceHelper;
import com.walruspay.assignment.twitter_clone.utils.Constants;
import com.walruspay.assignment.twitter_clone.utils.Constants.FollowAction;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class FollowerServiceImpl implements FollowerService {

	@Autowired
	private FollowerServiceHelper followerServiceHelper;

	@Autowired
	private CommonDao commonDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private FollowerDao followerDao;

	@Override
	public ServiceResponse<String> addFollower(FollowerRequest request) {
		ServiceResponse<String> response = new ServiceResponse<>();
		try {
			response = followerServiceHelper.validateFollowerRequest(request);
			if (null != response && null != response.getStatus()
					&& response.getStatus().equalsIgnoreCase(Constants.SUCCESS)) {
				Followers follower = followerDao.getFollowerByUserIdAndFollowerId(request.getUserId(),
						request.getFollowerId());
				if (request.getFollowAction().equals(FollowAction.FOLLOW.getName())) {
					if (null == follower) {
						follower = new Followers(request.getUserId(), request.getFollowerId());
						commonDao.saveObject(follower);
						log.info("Follower saved");
						response = new ServiceResponse<>("Follower saved", Constants.SUCCESS);
					}
				} else if (request.getFollowAction().equals(FollowAction.UNFOLLOW.getName())) {
					if (null == follower) {
						log.info("Follower not found");
						response = new ServiceResponse<>("Follower not found", Constants.FAILURE);
					} else {
						commonDao.delete(follower);
						log.info("Follower deleted");
						response = new ServiceResponse<>("Follower deleted", Constants.SUCCESS);
					}
				}
			}
		} catch (Exception e) {
			log.error(Constants.ERROR, e);
			response = new ServiceResponse<>(Constants.ERROR, Constants.FAILURE);
		}
		return response;
	}

}
