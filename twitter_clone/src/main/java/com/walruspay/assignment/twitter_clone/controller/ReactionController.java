package com.walruspay.assignment.twitter_clone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.walruspay.assignment.twitter_clone.models.ReactionRequest;
import com.walruspay.assignment.twitter_clone.models.ServiceRequest;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;
import com.walruspay.assignment.twitter_clone.service.ReactionService;

@RestController
@RequestMapping(value="v1/reaction")
public class ReactionController {

	@Autowired
	private ReactionService reactionService;
	
	@PostMapping("add")
	public ServiceResponse<String> addReaction(@RequestBody ServiceRequest<ReactionRequest> request){
		return reactionService.addReaction(request.getData());
	}
}
