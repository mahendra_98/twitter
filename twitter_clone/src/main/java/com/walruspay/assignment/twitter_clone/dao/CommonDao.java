package com.walruspay.assignment.twitter_clone.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class CommonDao {
	@Autowired
	private SessionFactory sessionFactory;

	public <T> void saveObject(T entityToSave) {
		if (entityToSave == null) {
			log.info("Object to be saved cannot be null");
			return;
		} else {
			Session session = sessionFactory.openSession();
			try {
				session.save(entityToSave);
			} catch (Exception e) {
				log.error(e);
			} finally {
				session.close();
			}
		}
	}

	public <T> void saveListObject(List<T> entityList) {
		if (null != entityList && !entityList.isEmpty()) {
			for (T ele : entityList) {
				saveObject(ele);
			}
		}
	}

	public <T> void updateObject(T entityToSave, Boolean flag) {
		if (entityToSave == null) {
			log.info("Object to be updated cannot be null");
			return;
		}
		Session session = sessionFactory.openSession();
		try {
			Transaction tx = session.beginTransaction();
			if (flag) {
				session.update(entityToSave);
			} else {
				session.saveOrUpdate(entityToSave);
			}
			tx.commit();
		} catch (Exception e) {
			log.error(e);
		} finally {
			session.close();
		}
	}

	public <T> void updateListObject(List<T> entityList, Boolean flag) {
		if (null != entityList && !entityList.isEmpty()) {
			for (T ele : entityList) {
				updateObject(ele, flag);
			}
		}
	}
	
	public <T> void delete(T entity){
		if(null!=entity){
			Session session=sessionFactory.openSession();
			try{
				Transaction tx=session.beginTransaction();
				session.delete(entity);
				tx.commit();
			}catch(Exception e){
				log.error("An error occurred",e);
			}finally{
				session.close();
			}
		}
	}
}
