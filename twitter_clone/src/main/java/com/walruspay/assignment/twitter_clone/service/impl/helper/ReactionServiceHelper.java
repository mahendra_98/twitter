package com.walruspay.assignment.twitter_clone.service.impl.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walruspay.assignment.twitter_clone.models.ReactionRequest;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;
import com.walruspay.assignment.twitter_clone.models.ValidateSessionRequest;
import com.walruspay.assignment.twitter_clone.service.SessionService;
import com.walruspay.assignment.twitter_clone.utils.Constants;
import com.walruspay.assignment.twitter_clone.utils.Constants.Reaction;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class ReactionServiceHelper {

	@Autowired 
	private SessionService sessionService;
	
	public ServiceResponse<String> validateAddReactionRequest(ReactionRequest request) {
		if (null == request) {
			log.info("Request cannot be empty");
			return new ServiceResponse<>("Request cannot be empty", Constants.FAILURE);
		}
		ServiceResponse<String> sessionResponse = sessionService.vaildateSession(new ValidateSessionRequest(request.getSessionKey()));
		if(!sessionResponse.getStatus().equals(Constants.SUCCESS)){
			log.info(sessionResponse.getMessage());
			return sessionResponse;
		}
		if (null == request.getUserId()) {
			log.info("User Id cannot be empty");
			return new ServiceResponse<>("User Id cannot be empty", Constants.FAILURE);
		}
		if (null == request.getPostId()) {
			log.info("Post ID cannot be Empty");
			return new ServiceResponse<>("Post ID cannot be Empty", Constants.FAILURE);
		}
		if (null == request.getReaction() || request.getReaction().isEmpty()) {
			log.info("Reaction cannot be empty");
			return new ServiceResponse<>("Reaction cannot be empty", Constants.FAILURE);
		}
		if (!Reaction.validateReaction(request.getReaction())) {
			log.info("Invalid reaction");
			return new ServiceResponse<>("Invalid reaction", Constants.FAILURE);
		}
		log.info("Request validated");
		return new ServiceResponse<>("Request validated", Constants.SUCCESS);
	}

}
