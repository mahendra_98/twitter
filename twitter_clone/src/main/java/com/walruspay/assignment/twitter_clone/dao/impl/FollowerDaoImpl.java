package com.walruspay.assignment.twitter_clone.dao.impl;

import javax.persistence.NoResultException;

import lombok.extern.log4j.Log4j2;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walruspay.assignment.twitter_clone.dao.FollowerDao;
import com.walruspay.assignment.twitter_clone.data.models.Followers;

@Service
@Log4j2
@SuppressWarnings("unchecked")
public class FollowerDaoImpl implements FollowerDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public Followers getFollowerByUserIdAndFollowerId(Integer userId, Integer followerId) {
		if (null == userId || null == followerId) {
			return null;
		}
		Session session = sessionFactory.openSession();
		try {
			Query<Followers> query = session.createQuery("from Followers where userId=:userId and followerId=:followerId");
			query.setParameter("userId", userId);
			query.setParameter("followerId", followerId);
			return query.getSingleResult();
		} catch (NoResultException e) {
			log.info("No result found");
		} catch (Exception e) {
			log.error("An error occurred", e);
		} finally {
			session.close();
		}
		return null;
	}
}

