package com.walruspay.assignment.twitter_clone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.walruspay.assignment.twitter_clone.models.LoginRequest;
import com.walruspay.assignment.twitter_clone.models.LoginResponse;
import com.walruspay.assignment.twitter_clone.models.LogoutRequest;
import com.walruspay.assignment.twitter_clone.models.RegisterUserRequest;
import com.walruspay.assignment.twitter_clone.models.RegisterUserResponse;
import com.walruspay.assignment.twitter_clone.models.SearchUserRequest;
import com.walruspay.assignment.twitter_clone.models.SearchUserResponse;
import com.walruspay.assignment.twitter_clone.models.ServiceRequest;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;
import com.walruspay.assignment.twitter_clone.service.UserService;

@RestController
@RequestMapping(value ="v1/user")
public class UserController {

	@Autowired
	private UserService userService;
	@PostMapping("register")
	public ServiceResponse<RegisterUserResponse> registerUser(@RequestBody ServiceRequest<RegisterUserRequest> request){
		return userService.registerUser(request.getData());
	}
	
	@PostMapping("login")
	public ServiceResponse<LoginResponse> login(@RequestBody ServiceRequest<LoginRequest> request){
		return userService.login(request.getData());
	}
	
	@PostMapping("logout")
	public ServiceResponse<String> logout(@RequestBody ServiceRequest<LogoutRequest> request){
		return userService.logout(request.getData());
	}
	@PostMapping("search")
	public ServiceResponse<List<SearchUserResponse>> searchUser(@RequestBody ServiceRequest<SearchUserRequest> request){
		return userService.searchUser(request.getData());
	}
}
