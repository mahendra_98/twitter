package com.walruspay.assignment.twitter_clone.dao.impl;

import javax.persistence.NoResultException;

import lombok.extern.log4j.Log4j2;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walruspay.assignment.twitter_clone.dao.SessionDao;
import com.walruspay.assignment.twitter_clone.data.models.SessionDetails;

@Service
@Log4j2
@SuppressWarnings("unchecked")
public class SessionDaoImpl implements SessionDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public SessionDetails getSessionBySessionKey(String sessionKey) {
		if (null == sessionKey || sessionKey.isEmpty()) {
			return null;
		}
		Session session = sessionFactory.openSession();
		try {
			Query<SessionDetails> query = session.createQuery("from SessionDetails where SESSION_KEY=:sessionKey and isActive=:isActive");
			query.setParameter("sessionKey", sessionKey);
			query.setParameter("isActive", Boolean.TRUE);
			return query.getSingleResult();
		} catch (NoResultException e) {
			log.info("No result found");
		} catch (Exception e) {
			log.error("An error occurred", e);
		} finally {
			session.close();
		}
		return null;
	}

}
