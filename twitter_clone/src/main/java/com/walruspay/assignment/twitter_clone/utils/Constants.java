package com.walruspay.assignment.twitter_clone.utils;

import lombok.Getter;

public interface Constants {
	String SUCCESS = "success";
	String FAILURE = "failure";
	String ERROR = "error";

	@Getter
	public enum Reaction {
		LIKE("Like"), UNLIKE("Unlike");

		private String name;

		private Reaction(String name) {
			this.name = name;
		}

		public static Boolean validateReaction(String reaction) {
			if (null == reaction || reaction.isEmpty()) {
				return false;
			}
			for (Reaction ele : Reaction.values()) {
				if (ele.getName().equalsIgnoreCase(reaction)) {
					return true;
				}
			}
			return false;
		}
	}

	@Getter
	public enum FollowAction {
		FOLLOW("Follow"), UNFOLLOW("Unfollow");

		private String name;

		private FollowAction(String name) {
			this.name = name;
		}

		public static Boolean validateFollowAction(String followAction) {
			if (null == followAction || followAction.isEmpty()) {
				return false;
			}
			for (FollowAction ele : FollowAction.values()) {
				if (ele.getName().equalsIgnoreCase(followAction)) {
					return true;
				}
			}
			return false;
		}
	}

}
