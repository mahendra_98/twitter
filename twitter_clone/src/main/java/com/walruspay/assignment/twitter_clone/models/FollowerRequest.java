package com.walruspay.assignment.twitter_clone.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class FollowerRequest {
	private String sessionKey;
	private Integer userId;
	private Integer followerId;
	private String followAction;
}
