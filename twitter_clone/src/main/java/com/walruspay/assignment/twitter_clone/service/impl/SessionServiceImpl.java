package com.walruspay.assignment.twitter_clone.service.impl;

import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.log4j.Log4j2;

import com.walruspay.assignment.twitter_clone.dao.CommonDao;
import com.walruspay.assignment.twitter_clone.dao.SessionDao;
import com.walruspay.assignment.twitter_clone.data.models.SessionDetails;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;
import com.walruspay.assignment.twitter_clone.models.ValidateSessionRequest;
import com.walruspay.assignment.twitter_clone.service.SessionService;
import com.walruspay.assignment.twitter_clone.utils.ConfigConstants;
import com.walruspay.assignment.twitter_clone.utils.Constants;
@Log4j2
@Service
public class SessionServiceImpl implements SessionService {

	@Autowired
	private ConfigConstants configConstnats;
	@Autowired
	private CommonDao commonDao;
	@Autowired
	private SessionDao sessionDao;
	
	@Override
	public ServiceResponse<String> generateSession() {
		ServiceResponse<String> serviceResponse = new ServiceResponse<>();
		try {
			Date date = new Date();
			// Session validity in minutes
			date.setTime(date.getTime() + (configConstnats.getSessionValidity() * 60 * 1000));
			SessionDetails session = new SessionDetails(UUID.randomUUID().toString().replaceAll("-", ""), date,Boolean.TRUE);
			commonDao.saveObject(session);
			log.info("Session generated.");
			serviceResponse = new ServiceResponse<>("Session generated.", Constants.SUCCESS, session.getSessionKey());
		} catch (Exception e) {
			log.error(e);
		}
		return serviceResponse;
	}

	@Override
	public ServiceResponse<String> vaildateSession(ValidateSessionRequest request) {
		ServiceResponse<String> serviceResponse = new ServiceResponse<>();
		try {
			SessionDetails session = sessionDao.getSessionBySessionKey(request.getSessionKey());
			if (null != session) {
				Date date = new Date();
				if (session.getValidTill().getTime() < date.getTime()) {
					serviceResponse= new ServiceResponse<>("Invalid session", Constants.FAILURE);
					log.info("Invalid session");
				}else{
					date.setTime(date.getTime() + (configConstnats.getSessionValidity() * 60 * 1000));
					session.setValidTill(date);
					commonDao.updateObject(session, Boolean.TRUE);
					log.info("Session validated.");
					serviceResponse = new ServiceResponse<>("Session validated.", Constants.SUCCESS, session.getSessionKey());
				}
			}else{
				serviceResponse= new ServiceResponse<>("Session not found.", Constants.FAILURE);
				log.info("Session not found.");
			}			
		} catch (Exception e) {
			log.error(e);
		}
		return serviceResponse;
	}

}
