package com.walruspay.assignment.twitter_clone.service.impl;

import java.util.List;

import com.walruspay.assignment.twitter_clone.dao.CommonDao;
import com.walruspay.assignment.twitter_clone.dao.PostDao;
import com.walruspay.assignment.twitter_clone.dao.UserDao;
import com.walruspay.assignment.twitter_clone.data.models.PostDeatils;
import com.walruspay.assignment.twitter_clone.data.models.User;
import com.walruspay.assignment.twitter_clone.models.FetchPostRequest;
import com.walruspay.assignment.twitter_clone.models.PostRequest;
import com.walruspay.assignment.twitter_clone.models.PostResponse;
import com.walruspay.assignment.twitter_clone.service.PostService;
import com.walruspay.assignment.twitter_clone.service.impl.helper.PostServiceHelper;
import com.walruspay.assignment.twitter_clone.utils.Constants;

import lombok.extern.log4j.Log4j2;

import com.walruspay.assignment.twitter_clone.models.ServiceResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PostServiceImpl implements PostService {

	@Autowired
	private PostServiceHelper postServiceHelper;
	@Autowired
	private PostDao postDao;
	@Autowired
	private CommonDao commonDao;
	@Autowired
	private UserDao userDao;
	
	@Override
	public ServiceResponse<String> createPost(PostRequest request) {
		ServiceResponse<String> serviceResponse = new ServiceResponse<>();
		try {
			serviceResponse = postServiceHelper.validateCreatePostRequest(request);
			if (serviceResponse.getStatus().equalsIgnoreCase(Constants.SUCCESS)) {
				PostDeatils postDeatils = new PostDeatils();
				if (null == request.getPostId()) {
					postDeatils = new PostDeatils(request.getUserId(), request.getPost());
					commonDao.saveObject(postDeatils);
					log.info("Post Saved");
					serviceResponse = new ServiceResponse<>("Post Saved", Constants.SUCCESS);
				} else {
					postDeatils = postDao.getPostByUserIdAndPostId(request.getUserId(), request.getPostId());
					if (null != postDeatils) {
						postDeatils.setPost(request.getPost());
						commonDao.updateObject(postDeatils, true);
						log.info("Post Updated");
						serviceResponse = new ServiceResponse<>("Post Updated", Constants.SUCCESS);
					}
				}
			}
		} catch (Exception e) {
			log.error(Constants.ERROR,e);
			serviceResponse = new ServiceResponse<>("An error occurred", Constants.FAILURE);
		}
		return serviceResponse;
	}
	
	@Override
	public ServiceResponse<List<PostResponse>> fetchAllPost(FetchPostRequest request) {
		ServiceResponse<List<PostResponse>> serviceResponse = new ServiceResponse<>();
		try {
			serviceResponse = postServiceHelper.validateFetchPostRequest(request);
			if(serviceResponse.getStatus().equals(Constants.SUCCESS)){
				User user = userDao.getUserByUserId(request.getUserId());
				if (null != user) {
					List<PostDeatils> postList = postDao.getAllPostsByUserId(request.getUserId());
					if (!postList.isEmpty()) {
						serviceResponse = new ServiceResponse<>("Posts fetched.", Constants.SUCCESS,postServiceHelper.preparePostResponse(postList));
						log.info("Posts fetched for userId. ", user.getUserId());
					} else {
						serviceResponse = new ServiceResponse<>("Posts not found.", Constants.FAILURE);
						log.info("Posts not found for userId. ", user.getUserId());
					}
				} else {
					serviceResponse = new ServiceResponse<>("User not found.", Constants.FAILURE);
					log.info("User not found for userId. ", request.getUserId());
				}
			}
		} catch (Exception e) {
			log.error(Constants.ERROR,e);
			serviceResponse = new ServiceResponse<>("An error occurred", Constants.FAILURE);
		}
		return serviceResponse;
	}

	

}
