package com.walruspay.assignment.twitter_clone.models;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class PostResponse {
	private String postContent;
	private String createdDate;
	private List<Integer> likes;
	private List<Integer> disLikes;
}
