package com.walruspay.assignment.twitter_clone.dao.impl;

import java.util.List;

import javax.persistence.NoResultException;

import lombok.extern.log4j.Log4j2;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walruspay.assignment.twitter_clone.dao.PostDao;
import com.walruspay.assignment.twitter_clone.data.models.PostDeatils;
@Service
@Log4j2
@SuppressWarnings("unchecked")
public class PostDaoImpl implements PostDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	public PostDeatils getPostByUserIdAndPostId(Integer userId, Integer postId) {
		if (null == userId && null == postId) {
			return null;
		}
		Session session = sessionFactory.openSession();
		try {
			Query<PostDeatils> query = session.createQuery("from PostDeatils where userId=:userId and postId=:postId");
			query.setParameter("userId", userId);
			query.setParameter("postId", postId);
			return query.getSingleResult();
		} catch (NoResultException e) {
			log.info("No result found");
		} catch (Exception e) {
			log.error("An error occurred", e);
		} finally {
			session.close();
		}
		return null;

	}

	@Override
	public List<PostDeatils> getAllPostsByUserId(Integer userId) {
		if (null == userId) {
			return null;
		}
		Session session = sessionFactory.openSession();
		try {
			Query<PostDeatils> query = session.createQuery("from PostDeatils where userId=:userId order by updatedOn DESC");
			query.setParameter("userId", userId);
			return query.getResultList();
		} catch (NoResultException e) {
			log.info("No result found");
		} catch (Exception e) {
			log.error("An error occurred", e);
		} finally {
			session.close();
		}
		return null;
	}

}
