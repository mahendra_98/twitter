package com.walruspay.assignment.twitter_clone.dao;

import com.walruspay.assignment.twitter_clone.data.models.SessionDetails;

public interface SessionDao {

	SessionDetails getSessionBySessionKey(String session);

}
