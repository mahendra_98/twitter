package com.walruspay.assignment.twitter_clone.service.impl.helper;

import java.util.ArrayList;
import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walruspay.assignment.twitter_clone.data.models.User;
import com.walruspay.assignment.twitter_clone.models.LoginRequest;
import com.walruspay.assignment.twitter_clone.models.LoginResponse;
import com.walruspay.assignment.twitter_clone.models.LogoutRequest;
import com.walruspay.assignment.twitter_clone.models.RegisterUserRequest;
import com.walruspay.assignment.twitter_clone.models.RegisterUserResponse;
import com.walruspay.assignment.twitter_clone.models.SearchUserRequest;
import com.walruspay.assignment.twitter_clone.models.SearchUserResponse;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;
import com.walruspay.assignment.twitter_clone.models.ValidateSessionRequest;
import com.walruspay.assignment.twitter_clone.service.SessionService;
import com.walruspay.assignment.twitter_clone.utils.Constants;
@Service
@Log4j2
public class UserServiceHelper {

	@Autowired 
	private SessionService sessionService;
	
	public ServiceResponse<RegisterUserResponse> validateRegisterUserRequest(RegisterUserRequest request) {
		if (null == request) {
			log.info("Request cannot be empty");
			return new ServiceResponse<>("Request cannot be empty", Constants.FAILURE);
		}
		ServiceResponse<String> sessionResponse = sessionService.vaildateSession(new ValidateSessionRequest(request.getSessionKey()));
		if(!sessionResponse.getStatus().equals(Constants.SUCCESS)){
			log.info(sessionResponse.getMessage());
			return new ServiceResponse<>(sessionResponse.getMessage(), sessionResponse.getStatus());
		}
		//TODO All the other validation can be added accordingly
		return new ServiceResponse<>("Request validated.", Constants.SUCCESS);
	}

	public ServiceResponse<LoginResponse> validateLoginRequest(LoginRequest request) {
		if (null == request) {
			log.info("Request cannot be empty");
			return new ServiceResponse<>("Request cannot be empty", Constants.FAILURE);
		}
		ServiceResponse<String> sessionResponse = sessionService.vaildateSession(new ValidateSessionRequest(request.getSessionKey()));
		if(!sessionResponse.getStatus().equals(Constants.SUCCESS)){
			log.info(sessionResponse.getMessage());
			return new ServiceResponse<>(sessionResponse.getMessage(), sessionResponse.getStatus());
		}
		//TODO All the other validation can be added accordingly
		return new ServiceResponse<>("Request validated.", Constants.SUCCESS);
	}

	public ServiceResponse<String> validateLogoutRequest(LogoutRequest request) {
		if (null == request) {
			log.info("Request cannot be empty");
			return new ServiceResponse<>("Request cannot be empty", Constants.FAILURE);
		}
		ServiceResponse<String> sessionResponse = sessionService.vaildateSession(new ValidateSessionRequest(request.getSessionKey()));
		if(!sessionResponse.getStatus().equals(Constants.SUCCESS)){
			log.info(sessionResponse.getMessage());
			return new ServiceResponse<>(sessionResponse.getMessage(), sessionResponse.getStatus());
		}
		//TODO All the other validation can be added accordingly
		return new ServiceResponse<>("Request validated.", Constants.SUCCESS);
	}

	public ServiceResponse<List<SearchUserResponse>> validateSearchUserRequest(SearchUserRequest request) {
		if (null == request) {
			log.info("Request cannot be empty");
			return new ServiceResponse<>("Request cannot be empty", Constants.FAILURE);
		}
		ServiceResponse<String> sessionResponse = sessionService.vaildateSession(new ValidateSessionRequest(request.getSessionKey()));
		if(!sessionResponse.getStatus().equals(Constants.SUCCESS)){
			log.info(sessionResponse.getMessage());
			return new ServiceResponse<>(sessionResponse.getMessage(), sessionResponse.getStatus());
		}
		//TODO All the other validation can be added accordingly
		return new ServiceResponse<>("Request validated.", Constants.SUCCESS);
	}

	/**
	 * This method prpares user response
	 * @param users
	 * @return
	 */
	public List<SearchUserResponse> prepareUserResponse(List<User> users) {
		List<SearchUserResponse> userResponse = new ArrayList<>();
		for (User ele : users) {
			userResponse.add(new SearchUserResponse(ele.getName(), ele.getUserId()));
		}
		return userResponse;
	}

}
