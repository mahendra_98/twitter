package com.walruspay.assignment.twitter_clone.service.impl;

import java.util.List;

import lombok.extern.log4j.Log4j2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.walruspay.assignment.twitter_clone.dao.CommonDao;
import com.walruspay.assignment.twitter_clone.dao.SessionDao;
import com.walruspay.assignment.twitter_clone.dao.UserDao;
import com.walruspay.assignment.twitter_clone.data.models.SessionDetails;
import com.walruspay.assignment.twitter_clone.data.models.User;
import com.walruspay.assignment.twitter_clone.models.LoginRequest;
import com.walruspay.assignment.twitter_clone.models.LoginResponse;
import com.walruspay.assignment.twitter_clone.models.LogoutRequest;
import com.walruspay.assignment.twitter_clone.models.RegisterUserRequest;
import com.walruspay.assignment.twitter_clone.models.RegisterUserResponse;
import com.walruspay.assignment.twitter_clone.models.SearchUserRequest;
import com.walruspay.assignment.twitter_clone.models.SearchUserResponse;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;
import com.walruspay.assignment.twitter_clone.service.UserService;
import com.walruspay.assignment.twitter_clone.service.impl.helper.UserServiceHelper;
import com.walruspay.assignment.twitter_clone.utils.Constants;
@Service
@Log4j2
public class UserServiceImpl implements UserService {

	@Autowired
	private UserServiceHelper userHelper;
	@Autowired
	private CommonDao commonDao;
	@Autowired
	private UserDao userDao;
	@Autowired
	private SessionDao sessionDao;
	
	@Override
	public ServiceResponse<RegisterUserResponse> registerUser(RegisterUserRequest request) {
		ServiceResponse<RegisterUserResponse>serviceResponse = new ServiceResponse<>();
		try {
			serviceResponse = userHelper.validateRegisterUserRequest(request);
			if(serviceResponse.getStatus().equals(Constants.SUCCESS)){
				BCryptPasswordEncoder enocoder = new BCryptPasswordEncoder();
				User user = new User(request.getName(), request.getMobile(), request.getEmail(), enocoder.encode(request.getPassword()));
				commonDao.saveObject(user);
				serviceResponse= new ServiceResponse<RegisterUserResponse>("User registered.", Constants.SUCCESS, new RegisterUserResponse(request.getSessionKey(), user.getUserId()));
			    log.info("User registered for userId. ",user.getUserId());
			}
		} catch (Exception e) {
			log.error(e);
			serviceResponse= new ServiceResponse<>("Something went wrong. Please try again.", Constants.ERROR);
		}
		return serviceResponse;
	}

	@Override
	public ServiceResponse<LoginResponse> login(LoginRequest request) {
		ServiceResponse<LoginResponse> serviceResponse = new ServiceResponse<>();
		try {
			serviceResponse = userHelper.validateLoginRequest(request);
			if (serviceResponse.getStatus().equals(Constants.SUCCESS)) {
				BCryptPasswordEncoder enocoder = new BCryptPasswordEncoder();
				User user = userDao.getUserByEmailOrMobile(request.getEmail(),request.getMobile());
				if (null != user) {
					if (enocoder.matches(request.getPassword(),user.getPassword())) {
						serviceResponse = new ServiceResponse<LoginResponse>("Login successful.", Constants.SUCCESS,
						new LoginResponse(request.getSessionKey(),user.getUserId()));
						log.info("Login successful for userId. ",user.getUserId());
					} else {
						serviceResponse = new ServiceResponse<>("Incorrect password.", Constants.FAILURE);
						log.info("Incorrect password for userId. ",user.getUserId());
						// TODO wrong attemp handelling can be added here 
					}
				} else {
					serviceResponse = new ServiceResponse<>("User not found.",Constants.FAILURE);
					log.info("User not found for email. ", request.getEmail());
				}
			}
		} catch (Exception e) {
			log.error(e);
			serviceResponse = new ServiceResponse<>("Something went wrong. Please try again.", Constants.ERROR);
		}
		return serviceResponse;
	}

	@Override
	public ServiceResponse<String> logout(LogoutRequest request) {
		ServiceResponse<String>serviceResponse = new ServiceResponse<>();
		try {
			serviceResponse= userHelper.validateLogoutRequest(request);
			if(serviceResponse.getStatus().equals(Constants.SUCCESS)){
				SessionDetails session = sessionDao.getSessionBySessionKey(request.getSessionKey());
				if(null!=session){
					session.setIsActive(Boolean.FALSE);
					commonDao.updateObject(session, Boolean.TRUE);
					serviceResponse = new ServiceResponse<>("Logout successful.", Constants.SUCCESS);
					log.info("Logout successful for session. ",session.getSessionKey());
				}else{
					serviceResponse = new ServiceResponse<>("Session not found.", Constants.FAILURE);
					log.info("Seeion not found for sessionKey. ",request.getSessionKey());
				}
			}
		} catch (Exception e) {
			log.error(e);
			serviceResponse = new ServiceResponse<>("Something went wrong. Please try again.", Constants.ERROR);
		}
		return serviceResponse;
	}

	@Override
	public ServiceResponse<List<SearchUserResponse>> searchUser(SearchUserRequest request) {
		ServiceResponse<List<SearchUserResponse>> serviceResponse = new ServiceResponse<>();
		try {
			serviceResponse = userHelper.validateSearchUserRequest(request);
			if(serviceResponse.getStatus().equals(Constants.SUCCESS)){
				List<User> users = userDao.getUsersByHandelName(request.getSearchString());
				if (!users.isEmpty()) {
				serviceResponse= new ServiceResponse<>("Users Fetched.", Constants.SUCCESS, userHelper.prepareUserResponse(users));
				log.info("Users fetched.");
				} else {
					serviceResponse = new ServiceResponse<>("Users not found.",Constants.FAILURE);
					log.info("Users not found.");
				}
			}
		} catch (Exception e) {
			log.error(e);
			serviceResponse = new ServiceResponse<>("Something went wrong. Please try again.", Constants.ERROR);
		}
		return serviceResponse;
	}

}
