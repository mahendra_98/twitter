package com.walruspay.assignment.twitter_clone.data.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "FOLLOWERS")
@Entity
@NoArgsConstructor
@Getter
@Setter
public class Followers implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column(name = "USER_ID", nullable = false)
	private Integer userId;
	@Column(name = "FOLLOWER_ID", nullable = false)
	private Integer followerId;
	@CreationTimestamp
	@Column(name = "CREATED_ON", nullable = false)
	private Timestamp createdOn;
	@UpdateTimestamp
	@Column(name = "UPDATED_ON", nullable = false)
	private Timestamp updatedOn;
	
	public Followers(Integer userId, Integer followerId) {
		this.userId = userId;
		this.followerId = followerId;
	}
	
	
}
