package com.walruspay.assignment.twitter_clone.dao.impl;

import javax.persistence.NoResultException;

import lombok.extern.log4j.Log4j2;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walruspay.assignment.twitter_clone.dao.ReactionDao;
import com.walruspay.assignment.twitter_clone.data.models.Reaction;
@Service
@Log4j2
@SuppressWarnings("unchecked")
public class ReactionDaoImpl implements ReactionDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Reaction getReactionByUserIdAndPostId(Integer userId, Integer postId) {
		if (null == userId && null == postId) {
			return null;
		}
		Session session = sessionFactory.openSession();
		try {
			Query<Reaction> query = session.createQuery("from Reaction where userId=:userId and postId=:postId");
			query.setParameter("userId", userId);
			query.setParameter("postId", postId);
			return query.getSingleResult();
		} catch (NoResultException e) {
			log.info("No result found");
		} catch (Exception e) {
			log.error("An error occurred", e);
		} finally {
			session.close();
		}
		return null;
	}

}
