package com.walruspay.assignment.twitter_clone.dao.impl;

import java.util.Collections;
import java.util.List;

import javax.persistence.NoResultException;

import lombok.extern.log4j.Log4j2;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walruspay.assignment.twitter_clone.dao.UserDao;
import com.walruspay.assignment.twitter_clone.data.models.User;

@Service
@Log4j2
@SuppressWarnings("unchecked")
public class UserDaoImpl implements UserDao {
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User getUserByEmailOrMobile(String email, String mobile) {
		if (null == email || email.isEmpty()) {
			return null;
		}
		Session session = sessionFactory.openSession();
		try {
			Query<User> query = session.createQuery("from User where EMAIL=:email");
//			query.setParameter("mobile", mobile);
			query.setParameter("email", email);
			return query.getSingleResult();
		} catch (NoResultException e) {
			log.info("No result found");
		} catch (Exception e) {
			log.error("An error occurred", e);
		} finally {
			session.close();
		}
		return null;
	}
	
	@Override
	public User getUserByUserId(Integer userId) {
		if (null == userId) {
			return null;
		}
		Session session = sessionFactory.openSession();
		try {
			Query<User> query = session.createQuery("from User where userId=:userId");
			query.setParameter("userId", userId);
			return query.getSingleResult();
		} catch (NoResultException e) {
			log.info("No result found");
		} catch (Exception e) {
			log.error("An error occurred", e);
		} finally {
			session.close();
		}
		return null;
	}

	@Override
	public List<User> getUsersByHandelName(String searchString) {
		if (null == searchString || searchString.isEmpty()) {
			return Collections.EMPTY_LIST;
		}
		Session session = sessionFactory.openSession();
		try {
			Query<User> query = session.createQuery("from User where name like: name");
			query.setParameter("name", "%" + searchString + "%");
			return query.getResultList();
		} catch (NoResultException e) {
			log.info("No result found");
		} catch (Exception e) {
			log.error("An error occurred", e);
		} finally {
			session.close();
		}
		return Collections.EMPTY_LIST;
	}

}
