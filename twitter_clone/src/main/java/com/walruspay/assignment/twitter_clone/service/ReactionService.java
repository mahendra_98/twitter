package com.walruspay.assignment.twitter_clone.service;

import com.walruspay.assignment.twitter_clone.models.ReactionRequest;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;

public interface ReactionService {

	public ServiceResponse<String> addReaction(ReactionRequest data);
}
