package com.walruspay.assignment.twitter_clone.service;

import com.walruspay.assignment.twitter_clone.models.ServiceResponse;
import com.walruspay.assignment.twitter_clone.models.ValidateSessionRequest;

public interface SessionService {

	ServiceResponse<String> generateSession();

	ServiceResponse<String> vaildateSession(ValidateSessionRequest data);

}
