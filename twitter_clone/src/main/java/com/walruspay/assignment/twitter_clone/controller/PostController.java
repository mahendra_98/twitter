package com.walruspay.assignment.twitter_clone.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.walruspay.assignment.twitter_clone.models.ServiceResponse;
import com.walruspay.assignment.twitter_clone.service.PostService;
import com.walruspay.assignment.twitter_clone.models.FetchPostRequest;
import com.walruspay.assignment.twitter_clone.models.PostRequest;
import com.walruspay.assignment.twitter_clone.models.PostResponse;
import com.walruspay.assignment.twitter_clone.models.ServiceRequest;

@RestController
@RequestMapping(value="v1/post")
public class PostController {

	@Autowired
	private PostService postService;
	
	@PostMapping("create")
	public ServiceResponse<String> createPost(@RequestBody ServiceRequest<PostRequest> request){
		return postService.createPost(request.getData());
	}
	@PostMapping("fetch/all/")
	public ServiceResponse<List<PostResponse>> fetchAllPost(@RequestBody ServiceRequest<FetchPostRequest> request){
		return postService.fetchAllPost(request.getData());
	}
}
