package com.walruspay.assignment.twitter_clone.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class RegisterUserRequest {
	private String sessionKey;
	private String name;
	private String email;
	private String mobile;
	private String password;
}
