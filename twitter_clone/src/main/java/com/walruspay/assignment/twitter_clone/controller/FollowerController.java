package com.walruspay.assignment.twitter_clone.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.walruspay.assignment.twitter_clone.models.FollowerRequest;
import com.walruspay.assignment.twitter_clone.models.ServiceRequest;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;
import com.walruspay.assignment.twitter_clone.service.FollowerService;

@RestController
@RequestMapping(value="v1/follower")
public class FollowerController {

	@Autowired
	private FollowerService followerService;
	
	@PostMapping("add")
	public ServiceResponse<String> addFollower(@RequestBody ServiceRequest<FollowerRequest> request) {
		return followerService.addFollower(request.getData());
	}

}
