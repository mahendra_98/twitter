package com.walruspay.assignment.twitter_clone.dao;

import java.util.List;

import com.walruspay.assignment.twitter_clone.data.models.User;

public interface UserDao {

	User getUserByEmailOrMobile(String email, String mobile);

	User getUserByUserId(Integer userId);

	List<User> getUsersByHandelName(String searchString);

}
