package com.walruspay.assignment.twitter_clone.data.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "REACTION")
@Entity
@NoArgsConstructor
@Getter
@Setter
public class Reaction implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "REACTION_ID")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer reactionId;
	@Column(name = "POST_ID", nullable = false)
	private Integer postId;
	@Column(name = "USER_ID", nullable = false)
	private Integer userId;
	@Column(name = "REACTION", nullable = false)
	private String reaction;
	@CreationTimestamp
	@Column(name = "CREATED_ON", nullable = false)
	private Timestamp createdOn;
	@UpdateTimestamp
	@Column(name = "UPDATED_ON", nullable = false)
	private Timestamp updatedOn;

	public Reaction(Integer postId, Integer userId, String reaction) {
		this.postId = postId;
		this.userId = userId;
		this.reaction = reaction;
	}

}
