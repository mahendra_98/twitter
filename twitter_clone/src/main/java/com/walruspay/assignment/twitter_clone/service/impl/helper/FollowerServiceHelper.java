package com.walruspay.assignment.twitter_clone.service.impl.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walruspay.assignment.twitter_clone.models.FollowerRequest;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;
import com.walruspay.assignment.twitter_clone.models.ValidateSessionRequest;
import com.walruspay.assignment.twitter_clone.service.SessionService;
import com.walruspay.assignment.twitter_clone.utils.CommonUtils;
import com.walruspay.assignment.twitter_clone.utils.Constants;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class FollowerServiceHelper {

	@Autowired
	private CommonUtils commonUtils;
	@Autowired
	private SessionService sessionService;

	public ServiceResponse<String> validateFollowerRequest(FollowerRequest request) {
		if (null == request) {
			log.info("Request cannot be empty");
			return new ServiceResponse<>("Request cannot be empty", Constants.FAILURE);
		}
		ServiceResponse<String> sessionResponse = sessionService.vaildateSession(new ValidateSessionRequest(request.getSessionKey()));
		if(!sessionResponse.getStatus().equals(Constants.SUCCESS)){
			log.info(sessionResponse.getMessage());
			return sessionResponse;
		}
		if (null == request.getUserId()) {
			log.info("User Id cannot be empty");
			return new ServiceResponse<>("User Id cannot be empty", Constants.FAILURE);
		}
		if (null == request.getFollowerId()) {
			log.info("Follower ID cannot be Empty");
			return new ServiceResponse<>("Follower ID cannot be Empty", Constants.FAILURE);
		}
		if (null == request.getFollowAction() || request.getFollowAction().isEmpty()) {
			log.info("Follow Action cannot be empty");
			return new ServiceResponse<>("Follow Action cannot be empty", Constants.FAILURE);
		}
		ServiceResponse<String> response = commonUtils.validateUser(request.getUserId());
		if (!response.getStatus().equals(Constants.SUCCESS)) {
			log.info("Invalid user Id");
			return new ServiceResponse<>("Invalid user Id", Constants.FAILURE);
		}
		response = commonUtils.validateUser(request.getFollowerId());
		if (!response.getStatus().equals(Constants.SUCCESS)) {
			log.info("Invalid Follower Id");
			return new ServiceResponse<>("Invalid Follower Id", Constants.FAILURE);
		}
		log.info("Request validated");
		return new ServiceResponse<>("Request validated", Constants.SUCCESS);
	}

}
