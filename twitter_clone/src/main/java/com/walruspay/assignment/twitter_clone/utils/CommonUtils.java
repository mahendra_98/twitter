package com.walruspay.assignment.twitter_clone.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.walruspay.assignment.twitter_clone.dao.CommonDao;
import com.walruspay.assignment.twitter_clone.dao.UserDao;
import com.walruspay.assignment.twitter_clone.data.models.User;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;

import lombok.extern.log4j.Log4j2;

@Component
@Log4j2
public class CommonUtils {

	@Autowired
	private CommonDao commonDao;
	@Autowired
	private UserDao userDao;

	public <T> ServiceResponse<T> validateUser(Integer userId) {
		if (null == userId) {
			log.info("User Id empty");
			return new ServiceResponse<>("User Id empty", Constants.FAILURE);
		}
		User user = userDao.getUserByUserId(userId);
		if (null == user) {
			log.info("Invalid user Id");
			return new ServiceResponse<>("Invalid user Id", Constants.FAILURE);
		}
		log.info("Request validated");
		return new ServiceResponse<>("Request validated", Constants.SUCCESS);
	}

}
