package com.walruspay.assignment.twitter_clone.service.impl.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walruspay.assignment.twitter_clone.data.models.PostDeatils;
import com.walruspay.assignment.twitter_clone.models.FetchPostRequest;
import com.walruspay.assignment.twitter_clone.models.PostRequest;
import com.walruspay.assignment.twitter_clone.models.PostResponse;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;
import com.walruspay.assignment.twitter_clone.models.ValidateSessionRequest;
import com.walruspay.assignment.twitter_clone.service.SessionService;
import com.walruspay.assignment.twitter_clone.utils.Constants;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class PostServiceHelper {
	
	@Autowired 
	private SessionService sessionService;

	public ServiceResponse<String> validateCreatePostRequest(PostRequest request) {
		if(null==request) {
			log.info("Request cannot be empty.");
			return new ServiceResponse<>("Request cannot be empty.",Constants.FAILURE);
		}
		ServiceResponse<String> sessionResponse = sessionService.vaildateSession(new ValidateSessionRequest(request.getSessionKey()));
		if(!sessionResponse.getStatus().equals(Constants.SUCCESS)){
			log.info(sessionResponse.getMessage());
			return sessionResponse;
		}
		if(null==request.getUserId()) {
			log.info("User Id cannot be empty.");
			return new ServiceResponse<>("User Id cannot be empty.",Constants.FAILURE);
		}
		if(null==request.getPost() || request.getPost().isEmpty()) {
			log.info("Post content cannot be Empty.");
			return new ServiceResponse<>("Post content cannot be Empty.",Constants.FAILURE);
		}
		if(request.getPost().length()>140) {
			log.info("Post length cannot be greater than 140.");
			return new ServiceResponse<>("Post length cannot be greater than 140.",Constants.FAILURE);
		}
		log.info("Request validated.");
		return new ServiceResponse<>("Request validated.",Constants.SUCCESS);
	}

	public ServiceResponse<List<PostResponse>> validateFetchPostRequest(FetchPostRequest request) {
		ServiceResponse<String> sessionResponse = sessionService.vaildateSession(new ValidateSessionRequest(request.getSessionKey()));
		if(!sessionResponse.getStatus().equals(Constants.SUCCESS)){
			log.info(sessionResponse.getMessage());
			return new ServiceResponse<>(sessionResponse.getMessage(), sessionResponse.getStatus());
		}
		//TODO All the other validation can be added accordingly
		log.info("Request validated.");
		return new ServiceResponse<>("Request validated.",Constants.SUCCESS);
	}

	
	public List<PostResponse> preparePostResponse(List<PostDeatils> postList) {
		List<PostResponse> responseList = new ArrayList<>();
		for (PostDeatils ele : postList) {
			// TODO Likes and dislikes can be added to individual post latter
			responseList.add(new PostResponse(ele.getPost(), ele.getUpdatedOn().toString(), null, null));
		}
		return responseList;
	}
	

}
