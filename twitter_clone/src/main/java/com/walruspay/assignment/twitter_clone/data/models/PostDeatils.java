package com.walruspay.assignment.twitter_clone.data.models;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * @author mahen
 *
 */
@Table(name = "POST")
@Entity
@NoArgsConstructor
@Getter
@Setter
public class PostDeatils implements Serializable {
	private static final long serialVersionUID = 1L;
	@Column(name = "POST_ID")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer postId;
	@Column(name = "USER_ID", nullable = false)
	private Integer userId;
	@Column(name = "POST", nullable = false)
	private String post;
	/**
	 *Further Different types of post content can be added
	 *
	 */
	@CreationTimestamp
	@Column(name = "CREATED_ON", nullable = false)
	private Timestamp createdOn;
	@UpdateTimestamp
	@Column(name = "UPDATED_ON", nullable = false)
	private Timestamp updatedOn;
	
	public PostDeatils(Integer userId, String post) {
		this.userId = userId;
		this.post = post;
	}
	
	
}
