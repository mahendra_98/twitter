package com.walruspay.assignment.twitter_clone.models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Getter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ReactionRequest {
    private String sessionKey;
	private Integer postId;
	private Integer userId;
	private String reaction;
}
