package com.walruspay.assignment.twitter_clone.service;

import java.util.List;

import com.walruspay.assignment.twitter_clone.models.LoginRequest;
import com.walruspay.assignment.twitter_clone.models.LoginResponse;
import com.walruspay.assignment.twitter_clone.models.LogoutRequest;
import com.walruspay.assignment.twitter_clone.models.RegisterUserRequest;
import com.walruspay.assignment.twitter_clone.models.RegisterUserResponse;
import com.walruspay.assignment.twitter_clone.models.SearchUserRequest;
import com.walruspay.assignment.twitter_clone.models.SearchUserResponse;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;

public interface UserService {

	ServiceResponse<RegisterUserResponse> registerUser(RegisterUserRequest request);

	ServiceResponse<LoginResponse> login(LoginRequest request);

	ServiceResponse<String> logout(LogoutRequest request);

	ServiceResponse<List<SearchUserResponse>> searchUser(SearchUserRequest request);

}
