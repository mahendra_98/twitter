package com.walruspay.assignment.twitter_clone.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.walruspay.assignment.twitter_clone.dao.CommonDao;
import com.walruspay.assignment.twitter_clone.dao.PostDao;
import com.walruspay.assignment.twitter_clone.dao.ReactionDao;
import com.walruspay.assignment.twitter_clone.data.models.PostDeatils;
import com.walruspay.assignment.twitter_clone.data.models.Reaction;
import com.walruspay.assignment.twitter_clone.models.ReactionRequest;
import com.walruspay.assignment.twitter_clone.models.ServiceResponse;
import com.walruspay.assignment.twitter_clone.service.ReactionService;
import com.walruspay.assignment.twitter_clone.service.impl.helper.ReactionServiceHelper;
import com.walruspay.assignment.twitter_clone.utils.Constants;

import lombok.extern.log4j.Log4j2;

@Service
@Log4j2
public class ReactionServiceImpl implements ReactionService {

	@Autowired
	private ReactionServiceHelper reactionServiceHelper;

	@Autowired
	private CommonDao commonDao;
	@Autowired
	private PostDao postDao;
	@Autowired
	private ReactionDao reactionDao;

	@Override
	public ServiceResponse<String> addReaction(ReactionRequest request) {
		ServiceResponse<String> response = new ServiceResponse<>();
		try {
			response = reactionServiceHelper.validateAddReactionRequest(request);
			if (null != response && null != response.getStatus()
					&& response.getStatus().equalsIgnoreCase(Constants.SUCCESS)) {
				Reaction reaction = new Reaction();
				PostDeatils postDeatils = postDao.getPostByUserIdAndPostId(request.getUserId(), request.getPostId());
				if (null != postDeatils) {
					reaction = reactionDao.getReactionByUserIdAndPostId(request.getUserId(), request.getPostId());
					if (null != reaction) {
						reaction.setReaction(request.getReaction());
						commonDao.updateObject(reaction, true);
						log.info("Reaction updated");
						response = new ServiceResponse<>("Reaction updated", Constants.SUCCESS);
					} else {
						reaction = new Reaction(request.getPostId(), request.getUserId(), request.getReaction());
						commonDao.saveObject(reaction);
						log.info("Reaction saved");
						response = new ServiceResponse<>("Reaction saved", Constants.SUCCESS);
					}
				} else {
					log.info("Post not found");
					response = new ServiceResponse<>("Post not found", Constants.FAILURE);
				}
			}
		} catch (Exception e) {
			log.info("An error occurred");
			response = new ServiceResponse<>("An error occurred", Constants.FAILURE);
		}
		return response;
	}

}
