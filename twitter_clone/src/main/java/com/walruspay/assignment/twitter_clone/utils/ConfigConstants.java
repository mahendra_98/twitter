package com.walruspay.assignment.twitter_clone.utils;

import lombok.Getter;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class ConfigConstants {
	@Value("${session.validity}")
	private Long sessionValidity;
}
